## Referências Técnicas

1. Quais foram os últimos dois livros técnicos que você leu?
	Angular na Prática
	Análise e projeto de sistemas de informação orientados a objetos


2. Quais foram os últimos dois framework javascript que você trabalhou?
	AngularJs
	Jquery

3. Descreva os principais pontos positivos de seu framework favorito.
	Agilidade para criação de aplicações
	Separação das responsabilidade de back End e Front
	Não deixa javascript "misturado" com html

4. Descreva os principais pontos negativos do seu framework favorito.
	Há necessidade de conhecer bem Javascript.

5. O que é código de qualidade para você?
	Que atende os requisitos gastando o melhor processamento possível, porém legivel para outros desenvolvedores.


## Conhecimento de desenvolvimento

1. O que é GIT?
	Sistema de versionamento. Desenvolvido por Linus Torvalds e sua equipe. Permite que varias pessoas trabalhem no mesmo projeto, mantendo todos atualizados e com o mesmo código.

2. Descreva um workflow simples de trabalho utilizando GIT.
	master
	hotfixes
	release
	develop
	features


3. Como você avalia seu grau de conhecimento em Orientação a objeto?
	mediano

4. O que é Dependency Injection?
	Deixar cada classe com respossibilidades unicas, que impessa a dependência de outras classes, pois as possíveis mudanças não iram afetar além da classe que está se modificada.
	Não devem depender de detalhes.


5. O significa a sigla S.O.L.I.D?
	Single responsibility
	Open/closed
	Liskov substitution
	Interface segregation
	Dependency invertion 

6. O que é BDD e qual sua importância para o processo de desenvolvimento?
	Behavior Driven Development (Desenvolvimento Guiado por Comportamento), pode ser aplicada muito bem com metodologias ágeis.
	Permite um entendimento claro entre todos os envolvidos no projeto.
	Descrição de comportamento que a aplicação deverá realizar.
	Feedback rápidos. 	

7. Fale sobre Design Patterns.
	São soluções comuns para resolver problemas.
	Soluções documentos e usados por diversas pessoas, o que garante que a problema de fato serão resolvidos. 
	São algoritmos reutilizáveis.

9. Discorra sobre práticas que você considera boas no desenvolvimento javascript.
	Separação das funções em arquivos únicos, permite testes, atribui para a função responsabilidade única e especificas.
	Evitar, sempre que possível, misturar HMTL com JavaScript ou CSS com JavaScript, ou alguma linguagem back end.
	Seguir sempre um padrão definido para o projeto, como nomenclaturas, objetos globais, etc.
	Refatorar sempre que possível.
	Cada vez mais se torna importante escrever testes.
