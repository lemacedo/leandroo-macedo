
describe("Testar função validaData", function(){

	it("Retorna dia", function(){
		expect(validaData("11/12/2014",'dia')).toBe('11');
		expect(validaData("01/12/2014",'dia')).toBe('01');
		expect(validaData("31/12/2014",'dia')).not.toBe('30');
	});

	it("Retorna mes", function(){
		expect(validaData("09/12/2014",'mes')).toBe('12');
		expect(validaData("09/00/2000",'mes')).toBe('00');
		expect(validaData("09/xx/2014",'mes')).not.toBe('12');
	});

	it("Retorna ano", function(){
		expect(validaData("09/12/2014",'ano')).toBe('2014')
	});

	it("Dias com no máximo 30 dias", function(){
		expect(validaData("32/04/2014",'')).toBe('Mês Errado.')
		expect(validaData("30/04/2014",'')).not.toBe('Mês Errado.')
		expect(validaData("29/01/2014",'')).not.toBe('Mês Errado.')
	});

	it("28 dias de fevereiro", function(){
		expect(validaData("30/02/2014",'')).toBe("fevereiro tem no máximo 30 dias");
		expect(validaData("29/02/2014",'')).toBe("fevereiro tem no máximo 30 dias");
		expect(validaData("28/02/2014",'')).not.toBe("fevereiro tem no máximo 30 dias");
		expect(validaData("05/02/2014",'')).not.toBe("fevereiro tem no máximo 30 dias");
		expect(validaData("30/03/2014",'')).not.toBe("fevereiro tem no máximo 30 dias");
	});



});