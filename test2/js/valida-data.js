
function validaData(data){

	dia = data.substring(0,2);
	mes = data.substring(3,5);
	ano = data.substring(6,10);

	//Valida dia, de 1 até 31 dias
	if(dia < 1 || dia > 31){
		alert("Dia está errado");
	}

	//Valida mês, de 1 até 12 meses
	if(mes < 1 || mes > 12){
		alert("Mês errado");
	}

	//28 dias de fevereiro
	if(mes == 02){
		if(dia > 28){
			alert("Fevereiro tem 28 dias.")
		}
	}

	//Mes com 30 apenas
	if(mes == '04' || mes == '06' || mes == '09' || mes == '11'){
		if(dia > 30){
			alert("Mês de 30 dias.")
		}
	}


}

$(document).ready(function(){
	$("#data-modificacao").focus(function(){
		$("#data-modificacao").mask("99/99/9999");
	});


    $("#data-modificacao").blur(function(){
    	var data = $(this).val();
    	validaData(data);
    });
});

