
	
$(function(){

	//arrastar
	$('.draggable').draggable({
		connectToSortable: '#droppable',
		helper: 'clone',
		tolerance: 'fit'
	});

	//Soltar e reordenar
	$('#droppable').sortable({
		placeholder: 'placeholder', 
		active: function(event, ui){
			$("#droppable ").remove();
			$("#droppable").addClass("drog-drap");
		}
	});

	//lixeira
	$('.lixeira').droppable({
        hoverClass: 'lixeira-ativa',
        drop: function(event, ui) {
            $(ui.draggable).remove();
        }
    });

	//Salvar
	$('.salvar').click(function(){
		var valores = new Array();

		$('#droppable ').each(function(){
			valores.push($(this).html());
		});

		//o que quiser
		alert(valores);
	});
});


   
